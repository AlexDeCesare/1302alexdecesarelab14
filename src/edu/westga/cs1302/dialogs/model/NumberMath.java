package edu.westga.cs1302.dialogs.model;

/**
 * This class does all of the math given two input numbers
 * 
 * @author Alex DeCesare
 * @version 14-July-2020
 */

public class NumberMath {
	
	private int integerNumber;
	private double doubleNumber;
	
	/**
	 * The constructor for the Math class
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @param integerNumber the integerNumber to perform math on
	 * @param doubleNumber the doubleNumber to perform math on
	 */
	
	public NumberMath(int integerNumber, double doubleNumber) {
		
		this.integerNumber = integerNumber;
		this.doubleNumber = doubleNumber;
		
	}
	
	/**
	 * The getter for the first number
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the first number
	 */
	public int getIntegerNumber() {
		return this.integerNumber;
	}
	
	/**
	 * The getter for the second number
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the second number
	 */
	public double getDoubleNumber() {
		return this.doubleNumber;
	}
	
	/**
	 * Adds the first and second number together
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the two numbers added together
	 */
	public double addNumbers() {
		double addedNumbers = this.getIntegerNumber() + this.getDoubleNumber();
		
		return addedNumbers;
	}
	
	/**
	 * The toString method for the Math class
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the string representation of the math class
	 */
	
	@Override
	public String toString() {
		return "The First Number For Math: " + this.getIntegerNumber() + " The Second Number For Math: " + this.getDoubleNumber();
	}

}