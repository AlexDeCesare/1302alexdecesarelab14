package edu.westga.cs1302.dialogs.test;

import static org.junit.jupiter.api.Assertions.*;


import org.junit.jupiter.api.Test;

import edu.westga.cs1302.dialogs.model.NumberMath;

class TestNumberMathConstructor {

	@Test
	public void shouldAllowNumbersWellNegative() {
		
		NumberMath testMath = new NumberMath(-500, -500);
		
		assertEquals( "The First Number For Math: -500 The Second Number For Math: -500.0", testMath.toString());
	}
	
	@Test
	public void shouldAllowNumbersOneBelowZero() {
		
		NumberMath testMath = new NumberMath(-1, -1);
		
		assertEquals( "The First Number For Math: -1 The Second Number For Math: -1.0", testMath.toString());
	}
	
	@Test
	public void shouldAllowNumbersOneTenthsBelowZero() {
		
		NumberMath testMath = new NumberMath(-1, -.1);
		
		assertEquals( "The First Number For Math: -1 The Second Number For Math: -0.1", testMath.toString());
	}
	
	@Test
	public void shouldAllowNumbersOneAtZero() {
		
		NumberMath testMath = new NumberMath(0, 0);
		
		assertEquals( "The First Number For Math: 0 The Second Number For Math: 0.0", testMath.toString());
	}
	
	@Test
	public void shouldAllowNumbersOneTenthsAboveZero() {
		
		NumberMath testMath = new NumberMath(1, .1);
		
		assertEquals( "The First Number For Math: 1 The Second Number For Math: 0.1", testMath.toString());
	}
	
	@Test
	public void shouldAllowNumbersOneAboveZero() {
		
		NumberMath testMath = new NumberMath(1, 1);
		
		assertEquals( "The First Number For Math: 1 The Second Number For Math: 1.0", testMath.toString());
	}
	
	@Test
	public void shouldAllowNumbersWellAboveZero() {
		
		NumberMath testMath = new NumberMath(500, 500);
		
		assertEquals( "The First Number For Math: 500 The Second Number For Math: 500.0", testMath.toString());
	}


}
