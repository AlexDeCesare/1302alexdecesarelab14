package edu.westga.cs1302.dialogs.test;

import static org.junit.jupiter.api.Assertions.*;


import org.junit.jupiter.api.Test;

import edu.westga.cs1302.dialogs.model.NumberMath;

class TestAddNumbers {

	@Test
	public void shouldAddNumbersWellBelowZero() {
		NumberMath testMath = new NumberMath(-500, -500);
		
		assertEquals(-1000, testMath.addNumbers());
	}
	
	@Test
	public void shouldAddNumbersOneBelowZero() {
		NumberMath testMath = new NumberMath(-1, -1);
		
		assertEquals(-2, testMath.addNumbers());
	}
	
	@Test
	public void shouldAddNumbersOneTenthsBelowOneBelowZero() {
		NumberMath testMath = new NumberMath(-1, -0.9);
		
		assertEquals(-1.9, testMath.addNumbers());
	}
	
	@Test
	public void shouldAddNumbersOneHalfBelowOneBelowZero() {
		NumberMath testMath = new NumberMath(-1, -0.5);
		
		assertEquals(-1.5, testMath.addNumbers());
	}
	
	@Test
	public void shouldAddNumbersOneTenthsBelowZero() {
		NumberMath testMath = new NumberMath(-1, -0.1);
		
		assertEquals(-1.1, testMath.addNumbers());
	}
	
	@Test
	public void shouldAddNumbersAtZero() {
		NumberMath testMath = new NumberMath(0, 0);
		
		assertEquals(0, testMath.addNumbers());
	}
	
	@Test
	public void shouldAddNumbersOneTenthsAboveZero() {
		NumberMath testMath = new NumberMath(1, 0.1);
		
		assertEquals(1.1, testMath.addNumbers());
	}
	
	@Test
	public void shouldAddNumbersOneHalfAboveZero() {
		NumberMath testMath = new NumberMath(1, 0.5);
		
		assertEquals(1.5, testMath.addNumbers());
	}
	
	@Test
	public void shouldAddNumbersOneTenthsBelowOne() {
		NumberMath testMath = new NumberMath(1, 0.9);
		
		assertEquals(1.9, testMath.addNumbers());
	}
	
	@Test
	public void shouldAddNumbersOneAboveZero() {
		NumberMath testMath = new NumberMath(1, 1);
		
		assertEquals(2, testMath.addNumbers());
	}
	
	@Test
	public void shouldAddNumbersWellAboveZero() {
		NumberMath testMath = new NumberMath(500, 500);
		
		assertEquals(1000, testMath.addNumbers());
	}

}
