package edu.westga.cs1302.dialogs.view;

import java.util.ArrayList;

import edu.westga.cs1302.dialogs.model.NumberMath;
import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * The view model class for the application
 * 
 * @author Alex DeCesare
 * @version 16-July-2020
 */

public class DialogsViewModel {
	
	private static final int MINIMUM_FIRST_NUMBER_VALUE = 2;
	private static final int MAXIMUM_FIRST_NUMBER_VALUE = 20;
	private static final int MINIMUM_SECOND_NUMBER_VALUE = 1;
	private static final double MAXIMUM_SECOND_NUMBER_VALUE = 2.1;
	
	private static final int FIRST_NUMBER_INTERVAL = 2;
	private static final double SECOND_NUMBER_INTERVAL = .1;
	
	private NumberMath theMath;	
	private String theOutput;
	private int theFirstNumberInput;
	private double theSecondNumberInput;
	
	private ListProperty<Integer> firstNumberChoices;
	private ListProperty<Double> secondNumberChoices;
	
	private ArrayList<Integer> theFirstNumberChoicesToSet;
	private ArrayList<Double> theSecondNumberChoicesToSet;
	
	/**
	 * The constructor for the view model
	 * 
	 * @precondition none
	 * @postcondition none
	 */
	
	public DialogsViewModel() {
		
		this.setTheFirstNumbers();
		this.setTheSecondNumbers();	
		
	}
	
	/**
	 * gets the number first choices
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the first number choices
	 */
	
	public ListProperty<Integer> getTheFirstNumberChoices() {
		return this.firstNumberChoices;
	}
	
	/**
	 * Sets the first number choices
	 * 
	 * @precondition none
	 * @postcondition none
	 */
	
	public void setTheFirstNumberChoices() {
		
		for (int counter = MINIMUM_FIRST_NUMBER_VALUE; counter <= MAXIMUM_FIRST_NUMBER_VALUE; counter += FIRST_NUMBER_INTERVAL) {
			this.theFirstNumberChoicesToSet.add(counter);
		}
		
	}
	
	/**
	 * gets the number second choices
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the second number choices
	 */
	
	public ListProperty<Double> getTheSecondNumberChoices() {
		return this.secondNumberChoices;
	}
	
	/**
	 * Sets the second number choices
	 * 
	 * @precondition none
	 * @postcondition none
	 */
	
	public void setTheSecondNumberChoices() {
		
		for (double counter = MINIMUM_SECOND_NUMBER_VALUE; counter <= MAXIMUM_SECOND_NUMBER_VALUE; counter += SECOND_NUMBER_INTERVAL) {
			this.theSecondNumberChoicesToSet.add(this.roundDoubles(counter));
		}
		
	}
	
	/**
	 * Gets the math class
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the math class
	 */
	
	public NumberMath getTheMath() {
		return this.theMath;
	}
	
	/**
	 * Gets the first number input
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the first number input
	 */
	
	public int getFirstNumberInput() {
		return this.theFirstNumberInput;
	}
	
	/**
	 * Gets the first number input
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the first number input
	 */
	
	public double getSecondNumberInput() {
		return this.theSecondNumberInput;
	}
	
	/**
	 * Sets the first number input
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @param theFirstNumberInput the first number input
	 */
	
	public void setFirstNumberInput(int theFirstNumberInput) {
		this.theFirstNumberInput = theFirstNumberInput;
	}
	
	/**
	 * Gets the output
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the output
	 */

	public String getOutput() {
		return this.theOutput;
	}
	
	/**
	 * Sets the second number input
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @param theSecondNumberInput the second number input
	 */
	
	public void setSecondNumberInput(double theSecondNumberInput) {
		this.theSecondNumberInput = theSecondNumberInput;
	}
	
	/**
	 * Sets the output of the numbers added together
	 * 
	 * @precondition none
	 * @postcondition none
	 */
	
	public void setOutput() {
		this.theMath = new NumberMath(this.getFirstNumberInput(), this.getSecondNumberInput());
		this.getTheMath().addNumbers();
		this.theOutput = "The solution of the two numbers is: " + this.getTheMath().addNumbers();
	}
	
	private void setTheSecondNumbers() {
		this.theSecondNumberChoicesToSet = new ArrayList<Double>();
		this.setTheSecondNumberChoices();
		ObservableList<Double> secondIntegers = FXCollections.observableArrayList(this.theSecondNumberChoicesToSet);
		this.secondNumberChoices = new SimpleListProperty<Double>(secondIntegers);
	}

	private void setTheFirstNumbers() {
		this.theFirstNumberChoicesToSet = new ArrayList<Integer>();
		this.setTheFirstNumberChoices();
		ObservableList<Integer> firstIntegers = FXCollections.observableArrayList(this.theFirstNumberChoicesToSet);
		this.firstNumberChoices = new SimpleListProperty<Integer>(firstIntegers);
	}
	
	private double roundDoubles(double theDoubleToRound) {
		
		double roundedNumber = Math.round(theDoubleToRound * 100.0) / 100.0;
		
		return roundedNumber;
		
	}

}
