package edu.westga.cs1302.dialogs.view;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;

/**
 * The code behind method for the project
 * 
 * @author Alex DeCesare
 * @version 16-July-2020
 */

public class DialogsCodeBehind {

	private DialogsViewModel theViewModel;
	
    @FXML
    private ListView<Integer> firstNumberList;

    @FXML
    private ListView<Double> secondNumberList;
    
    @FXML
    private Label labelOutput;
    
    /**
     * The constructor for the code behind method
     * 
     * @precondition none
     * @postcondition none
     */
    
    public DialogsCodeBehind() {
    	this.theViewModel = new DialogsViewModel();
    }

    @FXML
    private void initialize() {
    	
    	this.firstNumberList.itemsProperty().bind(this.theViewModel.getTheFirstNumberChoices());
    	this.firstNumberListEventListener();
    	
    	this.secondNumberList.itemsProperty().bind(this.theViewModel.getTheSecondNumberChoices());
    	this.secondNumberListEventListener();
    }
    
    @FXML
    void handleExit(ActionEvent event) {

    	System.exit(0);
    	
    }
    
	private void secondNumberListEventListener() {
		this.secondNumberList.getSelectionModel().selectedItemProperty().addListener(
    		(observable, oldInteger, newDouble) -> {
    			this.theViewModel.setSecondNumberInput(newDouble);
    			this.theViewModel.setOutput();
    			this.labelOutput.setText(this.theViewModel.getOutput());
    		});
	}

	private void firstNumberListEventListener() {
		this.firstNumberList.getSelectionModel().selectedItemProperty().addListener(
    		(observable, oldInteger, newInteger) -> {
    			this.theViewModel.setFirstNumberInput(newInteger);
    			this.theViewModel.setOutput();
    			this.labelOutput.setText(this.theViewModel.getOutput());
    		});
	}
}
